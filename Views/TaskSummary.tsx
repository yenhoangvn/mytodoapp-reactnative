import React from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';
import Header from '../components/Header';
import Body from '../components/BodySection/Body';

export default function App({navigation}) {
  return (
    <View style={styles.container}>
      <Header />
      <Body navigation={navigation} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FAF4E8',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
