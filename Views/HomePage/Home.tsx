import React from 'react';
import { StyleSheet, Text, View, Image, Pressable} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Header from '../../components/Header';
import Body from '../../components/BodySection/Body';
// import Footer from './components/Footer';

export default function App({ navigation }) {
  return (
    <View style={styles.container}>
      <View style={styles.textIconContainer}>
        <Text
        style={styles.text1}
        >my</Text>
        <FontAwesome
        style={styles.icon} 
        name='calendar'
         />
      </View>
      <Text
      style={styles.text2}
      >
      To Do List
      </Text>
      
      <Image 
      style={styles.images}
      source={require('../../photos/todosbanner2.png')}
       />

      <Text
      style={styles.text3}
      >
      ----- Let's make your day more organized!
      </Text>

      <Text
      style={styles.text4}
      >
      We are here to plan everything together with you!
      </Text>
     
      <Pressable
      onPress={()=> navigation.navigate("TodosList")}
      style={styles.buttonStyle}
      >
        <Text style={styles.text5}>Start now!</Text>
      </Pressable>
      
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FAF4E8',
    alignItems: 'center',
    justifyContent: 'center',
    padding:'5%',
  },
  textIconContainer:{
    flexDirection:'row',
    marginBottom:5,
    marginTop:20
  },
  images:{
    marginTop:'-53%',
    marginRight:'32%'
  },
  text1:{
    fontSize:22,
    color:'#85A494',
    fontFamily: "Cochin"
  },
  icon:{
    fontSize:20,
    color:'#85A494',
    paddingLeft:10
  },
  text2:{
    fontSize:28,
    color:'#85A494',
    fontFamily:'Arial',
    fontWeight: "bold",
    marginBottom:'10%'
  },
  text3:{
    color:'#85A494',
    fontSize:25,
    fontWeight: "bold",
    marginTop:'-5%'
  },
  text4:{
    fontSize:16,
    color:'#85A494',
    fontFamily: "Cochin",
    marginTop:10
  },
  text5:{
    color:'#85A494',
    fontSize:16,
    fontWeight: "bold",
  },
  buttonStyle:{
    marginTop:20,
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 12,
    paddingHorizontal: 50,
    borderRadius: 30,
    elevation: 3,
    backgroundColor: '#F3D4CC',
    marginBottom:20
  }
});
