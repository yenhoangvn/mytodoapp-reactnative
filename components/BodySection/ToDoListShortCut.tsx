import React, {useState} from 'react';
import { StyleSheet, Text, View, Image, SafeAreaView, FlatList, TouchableOpacity, Button } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';


const TodoList = [
  {
    id:'todo0',
    title: 'Learning React Native',
    date: '13-07-2020',
    duration: '13:00 - 18:00 pm',
    important:'true'

  },
  {
    id:'todo1',
    title: 'Playing video games',
    date: '13-07-2020',
    duration: '19:00 - 20:00 pm',
    important:'false'
  },
  {
    id:'todo2',
    title: 'Learning React Native',
    date: '13-07-2020',
    duration: '13:00 - 18:00 pm',
    important:'true'
  },
];
const Item = ({ item, onPress, textColor }) => (
  <TouchableOpacity onPress={onPress} style={{flexDirection:'row',alignItems:'center',paddingBottom:6}}>
    <Ionicons name='ios-heart-circle' color='#85A494' />
    <Text style={[styles.title, textColor]}>{`${item.title.split(" ")[0]} ${item.title.split(" ")[1]} ......`}</Text>
  </TouchableOpacity>
);


export default function ToDoListShortCut({navigation}) {

  const [selectedId, setSelectedId] = useState('todo0');
  const [index, setIndex] = useState(0);
  const renderItem = ({ item }) => {

    const color = item.id === selectedId ? '#D27968' : '#85A494';
    

    return (
      <Item
        item={item}
        onPress={() => {
          setSelectedId(item.id);
          let index = TodoList.findIndex(ele => ele.id===item.id)
          setIndex(index);
        }}
        textColor={{ color }}
      />
    );
  };
  return (
    <View style={styles.container}>
        <View style={{flexDirection:'column',maxWidth:'80%', paddingTop:'12%',paddingLeft:'8%',paddingRight:'5%',marginLeft:'10%',width:'70%',height:'60%',marginBottom:'-15%'}}>
            <View style={{flexDirection:'row',backgroundColor:'#F3D5CD',padding:'5%',borderRadius:15,alignItems:'center',justifyContent:'center',marginBottom:'10%',marginTop:'-50%'}}>
                <View
                style={{
                  height: 30,
                  width: 30,
                  borderRadius: 30,
                  justifyContent:'center',
                  alignItems:'center',
                  backgroundColor:'#C1C5C4',
                  paddingLeft:'0.8%',
                  marginRight:'8%'
                }}
                >
                  <Ionicons name='ios-add' color="#85A293" size={20} />
                </View>
                <Button 
                onPress={()=> navigation.navigate("Todo")}
                title="Add Todo" 
                color='#85A494'
                 />
            </View>    

      
            <SafeAreaView style={{backgroundColor:'#FAF4E8',borderRadius:15}}>
              <FlatList
                style={{marginBottom:8,padding:'5%'}}
                data={TodoList}
                renderItem={renderItem}
                keyExtractor={(item) => item.id}
                extraData={selectedId}
              />
            </SafeAreaView>
        </View>
        <View style={{backgroundColor:'#FAF4E8',borderRadius:15,paddingVertical:'10%',paddingHorizontal:'5%',marginTop:'-5%',marginRight:'5%',width:'40%'}}>
          <Text style={{color:'#85A494',fontSize:23,fontWeight:'600',marginBottom:10}}>Tuesday</Text>
          <Text style={{color:'#85A494',fontSize:15,fontWeight:'500',marginBottom:10}}>4 - August</Text>
          <Text style={styles.text}>{TodoList[index].title}</Text>

        </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    flexDirection:'row',
    backgroundColor: '#D3E1D4',
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft:'5%',
    marginRight:'5%',
    height:'53%',
    width:'80%',
    borderRadius:15,
    marginTop:10,
  },
  text:{
    color:'#85A494',
    fontSize:10
  },
  flexColumn:{
    flexDirection:'column',
    alignItems:'center',
    flex:1
  }
});
