import React from 'react';
import { StyleSheet, Text, View, Image, ScrollView } from 'react-native';
import TimeSection from './TimeSection';
import ToDoListSection from './ToDoListShortCut';

export default function App({ navigation }) {
  return (
    <View style={styles.container}>
      <Text style={styles.text1}>Hello sweety! </Text>
      <Text style={styles.text1}>What do you feel like to do today?</Text>
      <ToDoListSection navigation={ navigation } />
      <TimeSection />
      
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FAF4E8',
    alignItems: 'center',
    justifyContent: 'center',
  },
  text1:{
    color:'#8AA293',
    fontWeight:'500'
  }
});
