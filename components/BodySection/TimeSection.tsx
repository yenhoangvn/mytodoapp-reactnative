import React from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';
import Clock from './ClockContainer';
import QuoteTime from './QuoteClockContainer';

export default function App() {
  return (
    <View style={styles.container}>
      <Clock />
      <QuoteTime />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    flexDirection:'row',
    backgroundColor: '#FAF4E8',
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft:'5%',
    marginRight:'5%',
    height:'30%',
    marginBottom:30,
    marginTop:10
  },
});
