import React from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';

export default function App() {
  return (
    <View style={styles.container}>
      <Image 
      style={styles.images}
      source={require('../../photos/digitalClock.png')}
       />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#D3E1D4',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius:15,
    width:'50%',
    marginRight:'5%',
  },
  images:{
      width:'96%',
      height:'90%'
  }
});
