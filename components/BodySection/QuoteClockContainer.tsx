import React from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';

export default function App() {
  return (
    <View style={styles.container}>
        <Text
        style={styles.textQuoteMark}
        >
        ❝</Text>
      <Text
      style={styles.text}
      >I must govern the clock, not be governed by it </Text>
      <Text
      style={styles.textAuthor}
      >- Golda Meir</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#D3E1D4',
    width:'100%',
    height:'90%',
    paddingTop:'10%',
    paddingLeft:'5%',
    borderRadius:15,
  },
  text:{
    fontSize:13,
    color:'#85A494'
  },
  textAuthor:{
    fontSize:10,
    color:'#85A494'
  },
  textQuoteMark:{
    marginTop:'-15%',
    fontSize:20,
    color:'#85A494'
  }
});
