import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Home from './Views/HomePage/Home';
import TodosList from './Views/TaskSummary';
import Todo from './Views/TaskDetails';

const Tab = createBottomTabNavigator();

export default function App(){
   return(
    <NavigationContainer>
      <Tab.Navigator
        screenOptions={({ route }) => ({
          tabBarIcon: ({ focused, color, size }) => {
            let iconName;

            if (route.name === 'Home') {
              iconName = focused ? 'ios-home-outline': 'ios-home';
              return <Ionicons name={iconName} size={size} color={color} />;
            } else if (route.name === 'TodosList') {
              iconName = focused ? 'ios-list-circle-outline' : 'ios-list-circle';
              return <Ionicons name={iconName} size={size} color={color} />;
            }else if(route.name === 'Todo'){
              
              // iconName = focused ? 'ios-add-outline' : 'ios-add-circle-outline';
              return <View
              style={{
                flex:1,
                position: 'absolute',
                bottom: 10, // space from bottombar
                height: 50,
                width: 50,
                borderRadius: 50,
                justifyContent:'center',
                alignItems:'center',
                backgroundColor:'#BFDACC',
                borderStyle:'solid',
                borderWidth:3,
                borderColor:'#FAF4E8',
                paddingLeft:'5%'
            }}
            >
            <Ionicons name='ios-add' color="#85A293" size={35}/>
            </View>
            }

            // You can return any component that you like here!

          },
        })}
        tabBarOptions={{
          activeTintColor: '#85A494',
          inactiveTintColor: '#fff',
          style: {
            backgroundColor: '#F3D5CD',
            borderTopLeftRadius:15,
            borderTopRightRadius:15,
            justifyContent:'center',
            alignItems:'center',
            paddingTop:'2%'
          }
        }}
      >
        <Tab.Screen name="Home" component={Home} />
        <Tab.Screen name="Todo" component={Todo}
        // options={{ tabBarBadge: 3 }}
        />
        <Tab.Screen name="TodosList" component={TodosList} />
      </Tab.Navigator>
    </NavigationContainer>
 )
}
 const styles = StyleSheet.create({
   container: {
     marginTop: 25,
     padding: 10
   }
 });
 
//  AppRegistry.registerComponent("MyApp", () => App);